# Bienvenidos al repositorio de Recetas

Este proyecto contiene una API con los siguientes endpoint para alimentar una base
de Datos Mongodb, los endpoint son:

- Ruta: localhost:3000/new_user
- Ruta: localhost:3000/new_recipe
- Ruta: localhost:3000/recipesbyingredient
- Ruta: localhost:3000/recipes

------------

🤗💪¡Muchas Gracias!💪🤗
