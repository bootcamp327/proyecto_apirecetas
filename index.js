const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user')
const recipeSchema = require('./models/recipe')
      
const app = express();

// Middleware
app.use(express.json())

// MongoDB Connection
mongoose.connect('mongodb://poli:password@mongopoli:27017/miapp?authSource=admin')
.then(() => console.log('Conectado a MongoDB...'))
.catch((error) => console.error(error))

app.get('/', (req, res) => {
    res.send('Hola Mundo ;)');
})

app.post('/new_user', (req, res) => {
    const user = userSchema(req.body);
    user.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error));
})

app.post('/new_recipe', (req, res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error));
})

/*
app.post('/rate', (req, res) => {
    const { recipeId, userId, rating } = req.body;
    console.log(recipeId, userId, rating);
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            { $set: { avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    )
    .then( (data) => res.json(data))
    .catch( (error) => {
        console.log(error)
        res.send(error)
    })
})
*/

app.get('/recipes', (req, res) => {
    const { userId } = req.body;
    if(userId){
        recipeSchema
        .find ({ userId : userId })
        .then ( (data) => res.json(data))
        .catch((error) => res.json({message: error})) 
    }else{
        res.send('Es necesario indicar el UserId!');
    }
})

app.get('/recipesbyingredient', (req, res) => {
   // { ingredients: [ { name: 'banana' }, { name: 'leche' } ] }
    const ingredients = req.body;
    let {ingredients: [...nombres]} = ingredients;

    //res.send(nombres);
    
    if(nombres){
        recipeSchema
        .find({"ingredients.name": {$in:nombres}})
        .then ( (data) => res.json(data))
        .catch((error) => res.json({message: error})) 
    }else{
        res.send('Es necesario indicar el o los Ingredientes!');
    }
})

app.listen(3000, () => console.log('Escuchando en el puerto 3000 ;-)'))
